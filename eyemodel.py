from scipy import zeros, signal, random
from itertools import repeat

class Face:
    def __init__(self, x, y, w, h):
        self.samples = 8
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        # self.center_x = x+w/2
        # self.center_y = y+w/2
        self.filter_b = signal.firwin(self.samples, 0.008)
        
        self.center_x, self.filter_x_z = signal.lfilter(self.filter_b, 1, list(repeat(x+w/2, self.samples)), zi=signal.lfilter_zi(self.filter_b, 1))
        self.center_y, self.filter_y_z = signal.lfilter(self.filter_b, 1, list(repeat(y+h/2, self.samples)), zi=signal.lfilter_zi(self.filter_b, 1))
    
    def update(self, x, y, w, h):
        self.center_x, self.filter_x_z = signal.lfilter(self.filter_b, 1, [x+w/2], zi=self.filter_x_z)
        self.center_y, self.filter_y_z = signal.lfilter(self.filter_b, 1, [y+h/2], zi=self.filter_y_z)
        # self.w = w
        # self.h = h
        # print self.center_x, self.center_y, x, y

    def get_rect(self):
        return int(self.center_x[-1] - self.w/2), int(self.center_y[-1] - self.h/2), self.h, self.w

class EyesRect:
    def __init__(self, left, right):
        self.samples = 8
        self.left = left
        self.right = right
        self.x = left[0]
        self.y = left[1]
        self.w = self.right[0] + self.right[2] - self.x
        self.h = int((self.left[3] + self.right[3])/2)
        self.center_x = self.x+self.w/2
        self.center_y = self.y+self.h/2
        # print self.center_x, self.center_y
        self.filter_b = signal.firwin(self.samples, 0.08)
        self.center_x, self.filter_x_z = signal.lfilter(self.filter_b, 1, list(repeat(self.center_x, self.samples)), zi=signal.lfilter_zi(self.filter_b, 1))
        self.center_y, self.filter_y_z = signal.lfilter(self.filter_b, 1, list(repeat(self.center_y, self.samples)), zi=signal.lfilter_zi(self.filter_b, 1))

    def update(self, left, right):
        self.left = left
        self.right = right
        self.x = left[0]
        self.y = left[1]
        #self.w = self.right[0] + self.right[2] - self.x
        #self.h = int((self.left[3] + self.right[3])/2)
        self.center_x, self.filter_x_z = signal.lfilter(self.filter_b, 1, [self.x+self.w/2], zi=self.filter_x_z)
        self.center_y, self.filter_y_z = signal.lfilter(self.filter_b, 1, [self.y+self.h/2], zi=self.filter_y_z)
        # print self.center_x, self.center_y

    def get_rect(self):
        return int(self.center_x[-1] - self.w*1.2/2), int(self.center_y[-1] - (self.h*1.7)/2), int(self.w*1.2), int(self.h*1.7)
        # return self.x, self.y, self.w, self.h
        
class Point:
    def __init__(self, tp):
        self.x, self.y = tp

    def __str__(self):
        return "{}, {}".format(int(self.x), int(self.y))

    def __neg__(self):
        return Point((-self.x, -self.y))

    def __add__(self, point):
        return Point((self.x+point.x, self.y+point.y))

    def __sub__(self, point):
        return self + -point