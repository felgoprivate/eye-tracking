# based on https://github.com/kcmahip/opencv-eye-tracking/blob/master/eye_control.py
import numpy as np
import cv2
import time
from eyemodel import Face, EyesRect, Point

# cap = cv2.VideoCapture(0)  # initialize video capture
FILENAME = "../video_assets/Face_Recording.mp4"

cap = cv2.VideoCapture(FILENAME)

th_value = 5  # changeable threshold value
threshold=36

detector_params = cv2.SimpleBlobDetector_Params()

# detector_params.minThreshold = 20
# detector_params.maxThreshold = 210

detector_params.filterByArea = True
detector_params.maxArea = 1600
detector_params.minArea = 120

detector_params.filterByCircularity = True
detector_params.minCircularity = 0.3

# detector_params.filterByInertia = False
# detector_params.minInertiaRatio = 0.2

eyebrows_detector_params = cv2.SimpleBlobDetector_Params()
# eyebrows_detector_params.minThreshold = 0
# eyebrows_detector_params.maxThreshold = 254
eyebrows_detector_params.filterByCircularity = False
# eyebrows_detector_params.maxCircularity = 0.1
eyebrows_detector_params.filterByInertia = False
eyebrows_detector_params.maxInertiaRatio = 0.4
eyebrows_detector_params.maxInertiaRatio = 0
eyebrows_detector_params.filterByArea = True
eyebrows_detector_params.maxArea = 200000
# eyebrows_detector_params.minArea = 1000

detector = cv2.SimpleBlobDetector_create(detector_params)
eyebrows_detector = cv2.SimpleBlobDetector_create(eyebrows_detector_params)


frame_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
frame_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

left_eye_initial_pt = None
left_eye_pt = None

right_eye_initial_pt = None
right_eye_pt = None

def blob_process(img, threshold, detector):
    frame_h = int(img.shape[0])
    eyes_frame = img[int(frame_h*0.3):int(frame_h*0.7),:]
    eyebrows_frame = img[0:int(frame_h*0.45),:]
    
    eyebrows_frame = cv2.equalizeHist(cv2.GaussianBlur(eyebrows_frame, (9, 9), 0))
    # gray_frame = img
    # img = cv2.equalizeHist(img)
    # eyes_frame = cv2.GaussianBlur(eyes_frame, (3, 3), 0)
    cv2.imshow('img',eyes_frame)
    cv2.imshow('eyebrows',eyebrows_frame)

    _, eyes_frame = cv2.threshold(eyes_frame, threshold, 255, cv2.THRESH_BINARY)
    _, eyebrows_frame = cv2.threshold(eyebrows_frame, threshold, 255, cv2.THRESH_BINARY)

    cv2.imshow('eyebrows_th',eyebrows_frame)
    cv2.imshow('img_th',eyes_frame)

    # img = cv2.dilate(img, None, iterations=1)
    eyes_frame = cv2.erode(eyes_frame, None, iterations=2)
    eyebrows_frame = cv2.erode(eyebrows_frame, None, iterations=2)
    # cv2.imshow('img_erode',eyes_frame)
    # cv2.imshow('eyebrows_erode',eyebrows_frame)

    eyes_frame = cv2.dilate(eyes_frame, None, iterations=4)
    eyebrows_frame = cv2.dilate(eyebrows_frame, None, iterations=4)
    # cv2.imshow('img_dilatete',eyes_frame)
    # cv2.imshow('eyebrows_dilate',eyebrows_frame)

    eyes_frame = cv2.medianBlur(eyes_frame, 9)
    eyebrows_frame = cv2.medianBlur(eyebrows_frame, 7)
    cv2.imshow('img_blur',eyes_frame)

    keypoints = detector.detect(eyes_frame)
    eyebrows_contours = eyebrows_detector.detect(eyebrows_frame)
    # eyebrows_frame = np.invert(eyebrows_frame)
    eyebrows_contours, _ = cv2.findContours(eyebrows_frame, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    # contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # _, eyebrows_contours = cv2.findContours(eyebrows_frame, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(eyebrows_frame, eyebrows_contours, -1, (0,255,0), 3)
    cv2.imshow('eyebrows_blur',eyebrows_frame)

    for kp in keypoints:
        kp.pt = kp.pt[0], kp.pt[1]+int(frame_h*0.3)

    # print keypoints #, eyebrows_contours
    # print(map( lambda kp: kp.pt,keypoints))
    return keypoints, eyebrows_contours

face = None
eye_rect = None

font                   = cv2.FONT_HERSHEY_SIMPLEX
bottomLeftCornerOfText = (0,20)
fontScale              = 0.6
fontColor              = (255,255,255)
lineType               = 1

out = None #cv2.VideoWriter('output.avi', -1, 24.0, (640,480))

while 1:
    ret, frame = cap.read()
    # cv2.line(frame, (320, 0), (320, 480), (0, 200, 0), 2)
    # cv2.line(frame, (0, 200), (640, 200), (0, 200, 0), 2)

    if ret == True:

        frame = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
        frame = cv2.resize(
            frame, (int(frame_height/2), int(frame_width/2)))
        col = frame

        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

        pupilFrame = frame
        clahe = frame
        blur = frame
        edges = frame
        eyes = cv2.CascadeClassifier('./venv/Lib/site-packages/cv2/data/haarcascade_eye.xml')
        face_cascade = cv2.CascadeClassifier('./venv/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml')
        # faces = face_cascade.detectMultiScale(frame, 1.3, 5)

        # if len(faces) < 1:
        #     continue

        # if face is None:
        #     face = Face(faces[0][0], faces[0][1], faces[0][2], faces[0][3])
        # else:
        #     face.update(faces[0][0], faces[0][1], faces[0][2], faces[0][3])
        
        # face_rect = face.get_rect()

        # if face_rect[0] < 0 or face_rect[1] < 1:
        #     continue

        # face_frame = frame[face_rect[1]:face_rect[1]+face_rect[3], face_rect[0]:face_rect[0]+face_rect[2]]

        # cv2.rectangle(col, (face_rect[0], face_rect[1]), (face_rect[0]+face_rect[2], face_rect[1]+face_rect[3]), (255, 255, 0), 2)
        # cv2.imshow('Capture', col)

        detected = eyes.detectMultiScale(frame, 1.3, 5)

        if len(detected) != 2:
            continue
        
        detected = sorted(detected, key= lambda eye: eye[0])

        if eye_rect is None:
            eye_rect = EyesRect(detected[0],detected[1])
        else:
            eye_rect.update(detected[0],detected[1])

        eye_x = map(lambda eye: int(eye[0]+eye[2]/2), detected)
        eye_y = map(lambda eye: int(eye[1]+eye[3]/2), detected)
        eye_y_mean = sum(eye_y)/2
        # face_middle = int(face_rect[3]/2)
        eyes_rect = eye_rect.get_rect()
        eyes_frame = frame[eyes_rect[1]:eyes_rect[1]+eyes_rect[3], eyes_rect[0]:eyes_rect[0]+eyes_rect[2]]
        # eyes_frame = cv2.equalizeHist(eyes_frame)
        
        # gray_roi = cv2.GaussianBlur(eyes_frame, (7, 7), 0)

        # pupilFrame = cv2.equalizeHist(eyes_frame)

        # eyes_blur = cv2.medianBlur(pupilFrame, 7)  # median blur
        # _, eyes_frame_thrd1 = cv2.threshold(eyes_blur, threshold, 255, cv2.THRESH_BINARY)
        keypoints, eyebrows_keypoints = blob_process(eyes_frame, 42, detector)
        for eye_kp in keypoints:
            if eye_kp.pt[0] < eyes_frame.shape[1]/2:
                if right_eye_initial_pt is None:
                    right_eye_initial_pt = Point(eye_kp.pt)
                right_eye_pt = Point(eye_kp.pt)
            else:
                if left_eye_initial_pt is None:
                    left_eye_initial_pt = Point(eye_kp.pt)
                left_eye_pt = Point(eye_kp.pt)

        if (left_eye_pt != None) and (right_eye_pt != None):
            result_points = 'R: ' + str(right_eye_initial_pt - right_eye_pt) + ' - L: ' + str(left_eye_initial_pt - left_eye_pt)
            cv2.putText(eyes_frame, result_points, 
                bottomLeftCornerOfText, 
                font, 
                fontScale,
                fontColor,
                lineType)
            print result_points, eyes_frame.shape, cap.get(cv2.CAP_PROP_FPS)


        eyes_frame_thrd = cv2.drawKeypoints(eyes_frame, keypoints, eyes_frame, (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        # cv2.drawContours(eyebrows_frame, eyebrows_keypoints, -1, (0,255,0), 3)
        # eyes_frame_thrd = cv2.drawKeypoints(eyes_frame_thrd, eyebrows_keypoints, eyes_frame_thrd, (127, 255, 0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        cv2.imshow('Eyes frame', eyes_frame)
        if out is None:
            out = cv2.VideoWriter('output.avi', cv2.VideoWriter_fourcc('M','J','P','G'), cap.get(cv2.CAP_PROP_FPS), (eyes_frame.shape[1], eyes_frame.shape[0]))
        out.write(eyes_frame_thrd)
        # cv2.imshow('Gray ROI', gray_roi)
        # cv2.imshow('clahe', clahe)
        # cv2.imshow('blur', eyes_blur)
        cv2.imshow('eyes_frame_thrd', eyes_frame_thrd)
        # cv2.imshow('eyes_frame_thrd1', eyes_frame_thrd1)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
cv2.destroyAllWindows()
out.release()